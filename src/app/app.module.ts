import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GetApiComponent } from './Components/get-api/get-api.component';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './Components/login/login.component';
import { RouterModule } from '@angular/router';
import { routDefinition } from './Routing';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TooltipModule} from 'primeng/tooltip';
import { HomeComponent } from './Components/home/home.component';
import { NewApiComponent } from './Components/new-api/new-api.component';


@NgModule({
  declarations: [
    AppComponent,
    GetApiComponent,
    LoginComponent,
    HomeComponent,
    NewApiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    AccordionModule,
    CheckboxModule,
    BrowserAnimationsModule,
    TooltipModule,
    RouterModule.forRoot(routDefinition, {enableTracing: true})  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
