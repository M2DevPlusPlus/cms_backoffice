import { Component, OnInit } from '@angular/core';
import { SwaggerService } from '../../Services/swagger.service';
import { System } from '../../Classes/System';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isLoad: boolean = false;

  constructor(private swaggerService: SwaggerService, private location: Router) { }
  isNew: boolean = false
  ngOnInit() {
    if (this.swaggerService.ApisList == undefined) {
      this.swaggerService.getAllApis().subscribe(
        (data: System[]) => {
          this.swaggerService.ApisList = data;
          this.isLoad = true;
        })
    }
    else
    this.isLoad = true;

  }
  getApi(id) {
    this.location.navigate(['EditApi', id])
  }
  AddApi() {
    this.location.navigate(['NewApi'])
  }
}