import { Component, OnInit, EventEmitter } from '@angular/core';
import { Api } from '../../Classes/Api';
import { SwaggerService } from '../../Services/swagger.service';
import { System } from '../../Classes/System';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-api',
  templateUrl: './new-api.component.html',
  styleUrls: ['./new-api.component.css']
})
export class NewApiComponent implements OnInit {
  closed: EventEmitter<any> = new EventEmitter();
  editApi: EventEmitter<any> = new EventEmitter();
  api: Api;
  loaded: boolean;
  sysId: number;

  constructor(private swaggerService: SwaggerService, private router: Router, private location: Location) { }
  ngOnInit() {
    this.api = new Api("", "", "")
  }
  AddApi() {
    debugger
    this.loaded = false;
    if (this.swaggerService.ApisList.findIndex(s => s.swaggerUrl == this.api.swaggerUrl) == -1) {
      this.swaggerService.GetSingleApi(this.api).subscribe(
        (data: any[]) => {
          debugger
          this.swaggerService.ApisList.push(data[0] as System)
          this.swaggerService.ComponentsPerApi[(data[0] as System).systemId] = data[1] as JSON
          this.sysId = (data[0] as System).systemId
          this.loaded = true
          alert("The Api Succesfully added")
          this.closed.emit();
          this.swaggerService.isNewApi=false
        })
    }
    else {
      alert("Api is alredy exist")
      this.loaded = true;
      this.closed.emit();

    }
  }
  // EditApi(){
  //   this.editApi.emit(this.sysId)
  // }
  // Close(){
  //   this.closed.emit()
  // }
}