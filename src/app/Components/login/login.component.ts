import { Component, OnInit } from '@angular/core';
import { SwaggerService } from 'src/app/Services/swagger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private swaggerService: SwaggerService, private router: Router) { }

  ngOnInit() {
  }
  Send() {
    this.router.navigate(['/Home'])
  }
}
