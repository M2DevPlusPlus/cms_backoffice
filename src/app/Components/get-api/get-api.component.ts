import { Component, OnInit } from '@angular/core';
import { SwaggerService } from 'src/app/Services/swagger.service';
import { Field } from 'src/app/Classes/Field';
import { ActivatedRoute, Params } from '@angular/router';
import { System } from 'src/app/Classes/System';
import { Api } from 'src/app/Classes/Api';

@Component({
  selector: 'app-get-api',
  templateUrl: './get-api.component.html',
  styleUrls: ['./get-api.component.css']
})
export class GetApiComponent implements OnInit {
  sysEdit: boolean = false;
  editDecs: string;
  editName: string
  editSys: string;
  editPathDes: string;
  editPathReturn: string;
  iseditDes: boolean = false;
  iseditReturn: boolean = false;
  iseditSys: boolean = false;
  isEditName: boolean
  load: boolean = false
  editField: number = -1
  components: any;
  system: System;
  onSaving: boolean = false
  hasToSave: boolean
  isLoad: boolean;
  spinner = "../../assets/Spinner.gif"
  isUpdate: boolean = false;
  constructor(private swaggerService: SwaggerService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params.subscribe((paramsFromUrl: Params) => {
      var id = paramsFromUrl.id
      if (this.swaggerService.ApisList == undefined) {
        this.swaggerService.goHome()
      }
      this.isLoad = false
      this.system = this.swaggerService.ApisList.find(s => s.systemId == id)
      if (this.system.pathTbl == undefined) {
        var sys = this.swaggerService.ApisList.find(a => a.systemId == id)
        var api: Api = new Api(sys.url, sys.swaggerUrl, sys.description)
        this.swaggerService.GetSingleApi(api).subscribe(
          (data: any[]) => {
            debugger
            this.system.pathTbl = (data[0] as System).pathTbl
            // this.swaggerService.ComponentsPerApi[id] = data[1] as JSON
            this.swaggerService.ComponentsPerApi[id] = data[1] as JSON
            this.components = data[1] as JSON
            this.isLoad = true;
          },
          err => {
            alert("Load Api failed, please try later.")
            this.swaggerService.goHome();
          }
        )
      }
      else {
        this.components = this.swaggerService.ComponentsPerApi[id];
        this.isLoad = true;
      }
    }
    )
  }
  initEdit() {
    this.editDecs = this.swaggerService.system.description;
    this.sysEdit = true;
  }
  Edit() {
    this.swaggerService.sysToEdit[this.swaggerService.system.systemId] = this.editDecs;
    this.swaggerService.system.description = this.editDecs;
    this.sysEdit = false;
    this.hasToSave = true;
  }
  editingField(field: Field) {
    this.editField = field.fieldId;
    this.editDecs = field.description
    this.hasToSave = true;
  }
  saveEditing(field: Field) {
    debugger
    this.hasToSave = true;
    field.description = this.editDecs;
    this.editField = -1;
    this.swaggerService.fieldsToEdit[field.fieldId] = this.editDecs;
    this.editDecs = ""
  }
  cancleEditing() {
    debugger
    this.editField = -1;
    this.editDecs = ""
  }
  saveAll() {
    this.isUpdate = false;
    this.onSaving = false;
    this.swaggerService.SaveAllChanges().subscribe(
      isSucc => {
        if (isSucc == 0) {
          alert("All changes saved successfully")
        } else {
          alert("There was a problem saving, " + isSucc + " of the changes were not saved")
        }
        this.swaggerService.initEditArrays();
        this.onSaving = false;
        this.hasToSave = false;
        // this.isUpdate = true;
      },
      err => {
        alert("failed on saving changes")
        this.onSaving = false;
        // this.isUpdate = true
      }
    );
  }
  deleteApi() {
    var x = confirm("are you sure deleting " + this.system.description || this.system.url + "?")
    if (x) {
      this.swaggerService.deleteSystem(this.system.systemId).subscribe(
        succ => {
          if (succ == true) {
            alert("succesfully delete Api");
            var sys = this.swaggerService.ApisList.find(a => a.systemId == this.system.systemId)
            var ind = this.swaggerService.ApisList.indexOf(sys)
            this.swaggerService.ApisList.splice(ind, 1)
            this.swaggerService.goHome()
          }
          else
            alert("failed to delete Api")

        },

        err => {
          alert("failed to delete Api")
        }

      )
    }
  }
}
