import { LoginComponent } from './Components/login/login.component';
import { GetApiComponent } from './Components/get-api/get-api.component';
import { HomeComponent } from './Components/home/home.component';
import { NewApiComponent } from './Components/new-api/new-api.component';

export const routDefinition = [
    { path: '', redirectTo: 'Home', pathMatch: 'full' },

    {
        path: 'EditApi/:id',
        component: GetApiComponent,
    },
    {
        path: 'Home',
        component: HomeComponent,
    },
    {
        path:'NewApi',
        component:NewApiComponent
    },

    {
        path: '',
        component: LoginComponent,
    }
]