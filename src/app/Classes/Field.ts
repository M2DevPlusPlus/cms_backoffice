export class Field{
    fieldId:number
    pathId:number
    fieldName:string
    fieldType:string
    description:string
    require:boolean=true
    fieldIn:string
}