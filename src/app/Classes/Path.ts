import { Field } from './Field'

export class Path{
    pathId:number
    sytemId:number
    url:string
    pathReturn:string
    description:string
    httpType:string
    component:string
    pathName:string
    fieldTbl:Array<Field>
}