
export class Api {

    /**
     *
     */
    constructor(
        public url: string,
        public swaggerUrl:string,
        public desc?:string
    ) {

    }
}