
export class Descs{

    constructor(
        public sysDescs:{ [id: string]: string; } ,
        public pathName:{ [id: string]: string; } ,
        public pathDesc:{ [id: string]: string; } ,
        public pathReturns:{ [id: string]: string; } ,
        public fieldDesc:{ [id: string]: string; } 
    ) {

    }
}