import { Path } from './Path'

export class System {
    systemId: number
    url: string
    description: string
    swaggerUrl:string
    pathTbl: Array<Path>
}