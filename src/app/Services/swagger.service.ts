import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { System } from '../Classes/System';
import { Api } from '../Classes/Api';
import { Descs } from '../Classes/Descs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SwaggerService {


  api: Api;
  fieldEdit: any;
  fieldsToEdit: { [id: string]: string; } = {}
  sysToEdit: { [id: string]: string; } = {}
  pathDescToEdit: { [id: string]: string; } = {}
  pathNameToEdit: { [id: string]: string; } = {}
  pathReturnToEdit: { [id: string]: string; } = {}
  ApisList: System[];
  isNewApi: boolean = false;
  ComponentsPerApi: { [sysId: number]: JSON; } = {};
  constructor(private http: HttpClient, private location:Router)
  {
    this.api = new Api("", "");
  }
  system: System
  components: any;
  getAllApis() {
    debugger;
    return this.http.get('api/Tester/GetAllSystems');

  }
  GetComponents(sys: System) {
    const api: Api = new Api(sys.url, sys.swaggerUrl);
    return this.http.post('/api/Swagger/GetComponentsOfApi', api);
  }
  GetSingleApi(api: Api) {
    debugger;
    return this.http.post('/api/Swagger/GetApi', api);
  }
  initEditArrays() {
    this.sysToEdit = {};
    this.pathDescToEdit = {};
    this.pathReturnToEdit = {};
    this.fieldsToEdit = {};
  }
  SaveAllChanges() {
    debugger;
    var desc = new Descs(this.sysToEdit, this.pathNameToEdit, this.pathDescToEdit, this.pathReturnToEdit, this.fieldsToEdit);
    return this.http.post("/api/Swagger/ChangeDesc", desc)
  }

  goHome() {
    this.location.navigateByUrl('/')
  }

  deleteSystem(systemId: number) {
    return this.http.get("/api/Swagger/DeleteSystem/"+systemId)

  }
}
